import { describe, it } from "mocha";
import { expect } from "chai";
import { hex_to_rgb, rgb_to_hex } from "../src/converter.js";

describe("RGB-to-HEX converter", () => {
    it("is a function", () => {
        expect(rgb_to_hex).to.be.a('function');
    });
    it("should return a string", () => {
        expect(rgb_to_hex(0,0,0)).to.be.a('string');
    });
    it("first character is a hashtag", () => {
        expect(rgb_to_hex(0,0,0)[0]).to.equal("#");
    });
    it("should convert RED value correctly", () => {
        expect(rgb_to_hex(  0,  0,  0).substring(0, 3)).to.equal("#00");
        expect(rgb_to_hex(255,  0,  0).substring(0, 3)).to.equal("#ff");
        expect(rgb_to_hex(136,  0,  0).substring(0, 3)).to.equal("#88");
        expect(rgb_to_hex(999,  0,  0).substring(0, 3)).to.not.equal("#88");
    });
    it("should convert GREEN value correctly", () => {
        expect(rgb_to_hex(0,   0,  0).substring(3, 5)).to.equal("00");
        expect(rgb_to_hex(0, 255,  0).substring(3, 5)).to.equal("ff");
        expect(rgb_to_hex(0, 136,  0).substring(3, 5)).to.equal("88");
        expect(rgb_to_hex(0, 999,  0).substring(3, 5)).to.not.equal("88");
    });
    it("should convert BLUE value correctly", () => {
        expect(rgb_to_hex(0, 0,   0).substring(5, 7)).to.equal("00");
        expect(rgb_to_hex(0, 0, 255).substring(5, 7)).to.equal("ff");
        expect(rgb_to_hex(0, 0, 136).substring(5, 7)).to.equal("88");
        expect(rgb_to_hex(0, 0, 999).substring(5, 7)).to.not.equal("88");
    });
    it("should convert value correctly", () => {
        expect(rgb_to_hex(255,  0,  0)).to.equal("#ff0000");
        expect(rgb_to_hex(  0,255,  0)).to.equal("#00ff00");
        expect(rgb_to_hex(  0,  0,255)).to.equal("#0000ff");
        expect(rgb_to_hex(136,  0,255)).to.equal("#8800ff");
    });
});

describe("HEX-to-RGB converter", () => {
    it("is a function", () => {
        expect(hex_to_rgb).to.be.a('function');
    });
    it("should return a string", () => {
        expect(hex_to_rgb("#000000")).to.be.a('string');
    });
    it("should start with 'rgb('", () => {
        expect(hex_to_rgb("#000000").substring(0,4)).to.equal("rgb(");
    });
    it("should end with ')'", () => {
        expect(hex_to_rgb("#000000").slice(-1)).to.equal(")");
    });
    it("should convert value correctly", () => {
        expect(hex_to_rgb("#ff0000")).to.equal("rgb(255,0,0)");
        expect(hex_to_rgb("#00ff00")).to.equal("rgb(0,255,0)");
        expect(hex_to_rgb("#0000ff")).to.equal("rgb(0,0,255)");
        expect(hex_to_rgb("#8809ff")).to.equal("rgb(136,9,255)");
    });
});
