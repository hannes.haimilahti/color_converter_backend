import express from "express";
import routes from "./routes.js";
import cors from "cors";

const app = express();
app.use(cors({
    origin: 'http://localhost:5173'
}));
app.use('/api/v1', routes);

const api_url = 'http://localhost:3000/api/v1';
app.listen(3000, () => console.log(
    `Server listening at ${api_url}`
));
