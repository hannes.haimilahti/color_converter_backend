/**
 * Padding hex component to be 2 characters long if necessary.
 * @param {string} comp 
 * @returns {string} two hexadecimal characters
 */
const pad = (comp) => {
    let padded = comp.length < 2 ? "0" + comp : comp;
    return padded;
}

/**
 * RGB-to-HEX conversion
 * @param {number} r 0-255
 * @param {number} g 0-255
 * @param {number} b 0-255
 * @returns {string} in hex color format eg. "#00ffc3"
 */
export const rgb_to_hex = (r, g, b) => {
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return "#" + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
};

/**
 * HEX-to-RGB conversion
 * @param {string} hex String in hex color format eg. #aacc03
 * @returns {string} color in RGB format eg. "rgb(136,0,15)"
 */
export const hex_to_rgb = (hex) => {
    const RGB_RED = parseInt(hex.substring(1, 3), 16);
    const RGB_GREEN = parseInt(hex.substring(3, 5), 16);
    const RGB_BLUE = parseInt(hex.substring(5, 7), 16);
    return `rgb(${RGB_RED},${RGB_GREEN},${RGB_BLUE})`;
}
